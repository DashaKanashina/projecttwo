package ru.unn.project2;
import java.util.Arrays;
public class MainOne {
  public static void main(String[] args) {
      double [] mas= {10,20,30,40,50,60,70,80,90};
      for (int i = 0; i < mas.length; i++) {
          mas[i]= mas[i]+mas[i]*0.1;
      }
      for (int i = mas.length - 1; i > 0; i--) {
          for (int j = 0; j < i; j++) {
              if (mas[j] < mas[j + 1]) {
                  double tmp = mas[j];
                  mas[j] = mas[j + 1];
                  mas[j + 1] = tmp;
              }
          }
      }
  String masStr = Arrays.toString(mas);
  System.out.println(masStr);
  }
}
